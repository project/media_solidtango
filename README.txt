CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Todo
 * Installation
 * Usage

INTRODUCTION
------------

Adapted from media_vimeo module kudos to them.

Media: Solidtango adds Solidtango as a supported media provider.

Tested with D7 and Media 7.x-1.x.

Current Maintainers:

  * Solidtango

TODO
----

Unused code and incorrect comments cleanup, also options, theme and others.

REQUIREMENTS
------------

Media: Solidtango has one dependency.

Contributed modules
 * Media Internet - A submodule of the Media module.

INSTALLATION
------------

Media: Solidtango can be installed via the standard Drupal installation process
(http://drupal.org/node/895232).

USAGE
-----

Media: Solidtango integrates the Solidtango video-platform with the Media module
to allow users to add and manage Solidtango videos and live streams as they
would any other piece of media.

Internet media can be added on the Web tab of the Add file page (file/add/web).
With Media: Solidtango enabled, users can add a Solidtango video/livestream
by entering its URL.
